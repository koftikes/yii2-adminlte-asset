<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use sbs\helpers\AdminLTE;
use sbs\helpers\Content;
use sbs\widgets\Alert;
use sbs\widgets\NavBarUser;
use yii\widgets\Breadcrumbs;
use rmrevin\yii\fontawesome\FA;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="fixed <?= AdminLTE::skinClass(); ?> sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">
    <header class="main-header">
        <?= Html::a(
            '<span class="logo-mini">' . FA::i('home') . '</span><span class="logo-lg">' . Yii::$app->name . '</span>',
            Yii::$app->homeUrl,
            ['class' => 'logo']
        ); ?>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only"><?= Yii::t('back', 'Toggle navigation'); ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <?= NavBarUser::widget([
                        'profileUrl' => '/user/profile',
                        'logoutUrl' => '/site/logout',
                    ]); ?>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar"></section>
    </aside>
    <div class="content-wrapper">
        <section class="content-header">
            <?= Content::pageHeader('Fixed Layout', 'Blank example to the fixed layout'); ?>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>
        <section class="content">
            <?= Alert::widget(); ?>
            <?= $content; ?>
        </section>
    </div>
    <footer class="main-footer">
        <div class="pull-right hidden-xs"><b><?= Yii::t('back', 'Version'); ?></b> <?= Yii::$app->version; ?></div>
        <strong>Copyright &copy; 2016 - <?= date('Y'); ?> <?= Html::a(Yii::$app->name, Yii::$app->homeUrl); ?>.</strong>
        <?= Yii::t('back', 'All rights reserved.'); ?>
    </footer>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
