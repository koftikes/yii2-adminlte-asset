<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\web\View;
use yii\bootstrap\ActiveForm;
use rmrevin\yii\fontawesome\FA;

$this->title = Yii::t('back', 'Sign In');
$fieldUsernameOptions = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => '{input}<span class="form-control-feedback">' . FA::i('user')->size(FA::SIZE_LARGE) . '</span>',
];
$fieldPasswordOptions = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => '{input}<span class="form-control-feedback">' . FA::i('lock')->size(FA::SIZE_LARGE) . '</span>',
];
?>
<div class="login-box">
    <div class="login-logo">
        <?= Html::a('<b>' . Yii::$app->name . '</b>', Yii::$app->homeUrl) ?>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg"><?= Yii::t('back', 'Sign in to start your session'); ?></p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'username', $fieldUsernameOptions)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('username'), 'autofocus' => true]); ?>

        <?= $form
            ->field($model, 'password', $fieldPasswordOptions)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]); ?>

        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <?= $form->field($model, 'rememberMe')->checkbox(); ?>
                </div>
            </div>
            <div class="col-xs-4">
                <?= Html::submitButton(
                    Yii::t('back', 'Sign In'),
                    ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']
                ); ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php $this->registerJs("
$(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
", View::POS_READY);
?>
